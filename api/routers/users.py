from fastapi import APIRouter, Depends, HTTPException
from typing import Optional
from queries.users import UserQueries, UserOut, UserIn, UserListOut
from psycopg.errors import ForeignKeyViolation

router = APIRouter()

@router.get("/api/users/{user_id}", response_model=Optional[UserOut])
def get_user(
    user_id: int,
    queries: UserQueries = Depends(),
):
    record = queries.get_user(user_id)
    if record is None:
        raise HTTPException(status_code=404, detail=f"No user found with id {user_id}")
    else:
        return record

@router.delete("/api/users/{user_id}", response_model=bool)
def delete_user(
    user_id: int, 
    queries: UserQueries = Depends()
):
    queries.delete_user(user_id)
    return True

@router.get("/api/users", response_model=UserListOut)
def get_users(
    queries: UserQueries = Depends()
):
    # returning a JSON object is conventional -- preferred instead of returning 
    # a list due to historical security reasons and flexibility
    return {"users": queries.get_all_users()}

@router.post("/api/users", response_model=UserOut)
def create_user(
    user: UserIn,    
    queries: UserQueries = Depends(),
):
    try: 
        return queries.create_user(user)
    except ForeignKeyViolation as e:
        raise HTTPException(status_code=400, detail="Failed to create user due to foreign key violation")

# 
# Resources
# routers.trucks example
# users.queries
# docs page (at http://localhost:8000/docs#)
# Notion: https://marbled-particle-5cf.notion.site/FastAPI-2eee765c870245ab9f28a3ef5456a981?pvs=4
# take note of endpoints best practices


